package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"strings"
	"time"
)

// Grocy structs
type User struct {
	Id                  int     `json:"id"`
	Username            string  `json:"username"`
	FirstName           string  `json:"first_name"`
	LastName            string  `json:"last_name"`
	RowCreatedTimestamp string  `json:"row_created_timestamp"`
	DisplayName         string  `json:"display_name"`
	PictureFileName     *string `json:"picture_file_name"`
}

type ChoreSummary struct {
	Id                            int    `json:"id"`
	ChoreId                       int    `json:"chore_id"`
	ChoreName                     string `json:"chore_name"`
	LastTrackedTime               string `json:"last_tracked_time"`
	NextEstimatedExecutionTime    string `json:"next_estimated_execution_time"`
	TrackDateOnly                 int    `json:"track_date_only"`
	NextExecutionAssignedToUserId int    `json:"next_execution_assigned_to_user_id"`
	IsRescheduled                 int    `json:"is_rescheduled"`
	IsReassigned                  int    `json:"is_reassigned"`
	NextExecutionAssignedUser     User   `json:"next_execution_assigned_user"`
}

type Chore struct {
	Id                  string `json:"id"`
	Name                string `json:"name"`
	Description         string `json:"description"`
	PeriodType          string `json:"period_type"`
	PeriodDays          string `json:"period_days"`
	RowCreatedTimestamp string `json:"row_created_timestamp"`
}

type UserSummary struct {
	Id                  string `json:"id"`
	Username            string `json:"username"`
	FirstName           string `json:"first_name"`
	LastName            string `json:"last_name"`
	DisplayName         string `json:"display_name"`
	RowCreatedTimestamp string `json:"row_created_timestamp"`
}

type ChoreDetails struct {
	Chore                      Chore       `json:"chore"`
	LastTracked                string      `json:"las_tracked"`
	LastDoneBy                 UserSummary `json:"last_done_by"`
	NextEstimatedExecutionTime string      `json:"next_estimated_execution_time"`
}

type Execution struct {
	TrackedTime string `json:"tracked_time"`
	DoneBy      int    `json:"done_by"`
	Skipped     bool   `json:"skipped"`
}

type ExecutionStrings struct {
	TrackedTime string `json:"tracked_time"`
	DoneBy      string `json:"done_by"`
	Skipped     string `json:"skipped"`
}

// Grocy commands
func addHeaders(req *http.Request) {

	req.Header.Add("accept", "application/json")
	req.Header.Add("GROCY-API-KEY", envVars[GROCY_API])
}

func grocyRequest(method string, endpoint string, body io.Reader) (*http.Response, error) {
	client := &http.Client{}

	chores, err := http.NewRequest(method, envVars[GROCY_URL]+endpoint, body)
	if err != nil {
		return nil, err
	}

	addHeaders(chores)

	if body != nil {
		chores.Header.Add("Content-Type", "application/json")
	}

	resp, err := client.Do(chores)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

func getChores() ([]ChoreSummary, error) {

	resp, err := grocyRequest(httpGet, "chores", nil)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("Unsuccessful response: %s", resp.Status)
	}

	defer resp.Body.Close()

	var choreData []ChoreSummary

	if err := json.NewDecoder(resp.Body).Decode(&choreData); err != nil {
		return nil, err
	}

	return choreData, nil
}

func getChoreDetails(choreId int) (*ChoreDetails, error) {

	resp, err := grocyRequest(httpGet, "chores/"+strconv.Itoa(choreId), nil)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var choreDetails ChoreDetails
	if err := json.NewDecoder(resp.Body).Decode(&choreDetails); err != nil {
		return nil, err
	}

	return &choreDetails, nil
}

func parseGrocyDate(grocyDate string) (time.Time, error) {
	dateSplit := strings.Split(grocyDate, " ")[0]
	if len(dateSplit) == 0 {
		return time.Time{}, fmt.Errorf("Failed to parse date from: %s", grocyDate)
	}

	parsed, err := time.Parse("2006-01-02", dateSplit)
	if err != nil {
		return time.Time{}, fmt.Errorf("Error parsing Grocy Date: %s", err)
	}

	return time.Date(parsed.Year(), parsed.Month(), parsed.Day(), 0, 0, 0, 0, locale), nil
}

func trackExecution(userId, choreId int, trackedtime string) error {

	endpoint := fmt.Sprintf("chores/%d/execute", choreId)

	// payload := Execution{
	// 	TrackedTime: trackedtime,
	// 	DoneBy: userId,
	// 	Skipped: false,
	// }
	payload := ExecutionStrings{
		TrackedTime: trackedtime,
		DoneBy:      strconv.Itoa(userId),
		Skipped:     "false",
	}

	payloadJson, _ := json.Marshal(payload)

	resp, err := grocyRequest(httpPost, endpoint, bytes.NewBuffer(payloadJson))
	if err != nil {
		return err
	}

	if resp.StatusCode != 200 {
		return fmt.Errorf("Unable to track execution in Grocy: %s", resp.Status)
	}

	return nil
}
