# Things that need to be done

- [ ] handle tasks with no due date
- [ ] Load calendar name from env
- [x] Load secrets from file instead of cli args
- [x] Create Dockerfile
- [ ] Deploy this alongside grocy


