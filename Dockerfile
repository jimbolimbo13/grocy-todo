FROM golang:1.18-alpine as builder
WORKDIR /app
COPY go.mod go.sum ./
RUN go mod download
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -o grocy-todo .


FROM alpine:latest
WORKDIR /app
COPY --from=builder /app/grocy-todo ./
RUN apk add tzdata
ENTRYPOINT ["./grocy-todo"]
