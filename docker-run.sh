#!/usr/bin/env sh

ENV_FILE=/path/to/.env

docker run --env-file $ENV_FILE --rm registry.gitlab.com/jimbolimbo13/grocy-todo:latest
