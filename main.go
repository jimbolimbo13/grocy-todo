package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"time"

	"log"

	caldav "github.com/emersion/go-webdav/caldav"
)

const (
	httpGet  = "GET"
	httpPost = "POST"

	GROCY_URL       = "GROCY_URL"
	GROCY_API       = "GROCY_API"
	CALDAV_URL      = "CALDAV_URL"
	CALDAV_USERNAME = "CALDAV_USERNAME"
	CALDAV_PASSW    = "CALDAV_PASSW"
	LOCALE          = "LOCALE"
)

var (
	envVarNames = []string{
		GROCY_URL,
		GROCY_API,
		CALDAV_URL,
		CALDAV_USERNAME,
		CALDAV_PASSW,
		LOCALE,
	}

	envVars = map[string]string{}

	env_file string
	locale   *time.Location
)

func handleArgs() {
	if len(os.Args) > 1 && (os.Args[1] == "-h" || os.Args[1] == "--help") {
		fmt.Println("The function takes positional parameters:")
		fmt.Println("env_file")
		os.Exit(0)
	}

	if len(os.Args) > 1 {
		env_file = os.Args[1]
		return
	}

	env_file = ".env"
}

func loadEnvFile() {

	file, err := os.Open(env_file)
	if err != nil {
		return
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	var next_var []string
	for scanner.Scan() {
		next_read := scanner.Text()
		if strings.Contains(next_read, "#") {
			next_read = next_read[:strings.Index(next_read, "#")]
		}

		next_var = strings.Split(next_read, "=")

		if len(next_var) != 2 {
			continue
		}
		envVars[next_var[0]] = next_var[1]
	}

}

func loadEnv() {

	// load variables from file first, these will take precedence
	loadEnvFile()

	for _, key := range envVarNames {
		if envVars[key] == "" {
			envVars[key] = os.Getenv(key)
		}

		if envVars[key] == "" {
			fmt.Printf("Unable to load %s from environment or file", key)
			os.Exit(1)
		}
	}
}

// main function
func main() {

	handleArgs()
	loadEnv()

	var err error
	locale, err = time.LoadLocation(envVars[LOCALE])
	if err != nil {
		fmt.Println("Failed to load timezone location. All time stamps will be incorrect!")
	}

	allChores, err := getChores()
	if err != nil {
		log.Fatal(err)
	}

	// create caldav client
	cdClient, err := initDavClient(envVars[CALDAV_USERNAME], envVars[CALDAV_PASSW])
	if err != nil {
		log.Fatal(err)
	}

	// "My Tasks" -- calendars[1]
	myTasksCalendar, err := getCaldavTodos(*cdClient)
	if err != nil {
		log.Fatal(err)
	}

	tasks, err := getAllTodos(*cdClient, myTasksCalendar)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(myTasksCalendar.Path)
	tasksByName := map[string]caldav.CalendarObject{}

	for _, task := range tasks {
		tasksByName[getTaskValue(task, "SUMMARY")] = task
	}

	// assume this runs daily at midnight +1 minute
	// Cases:
	// todo is marked complete
	// 	an entry needs to be made in grocy that is was done if:
	// 	 updated time on todo is more recent than last tracked time in grocy
	// 	 if grocy item is "trackDateOnly", then only compare the date
	// 	 equal date is not considered more recent
	//	todo DUE date does not match the next estimated tracking
	// 	 the todo must be marked incomplete
	// 	 DUE date on the todo must be updated
	// 	todo doesn't exist
	// 	 create it ( this is already done )

	for _, chore := range allChores {
		if found, ok := tasksByName[chore.ChoreName]; ok {
			fmt.Printf("Found chore %s in tasks: %s\n", chore.ChoreName, getTaskValue(found, "DUE"))

			// Case 1
			if getTaskValue(found, "COMPLETED") != "" {
				lastTracked, _ := parseGrocyDate(chore.LastTrackedTime)

				dateCompleted, err := parseDateTime(found, "COMPLETED")
				if err != nil {
					fmt.Printf("Encountered error parsing completed date: %s\n", err)
				} else if chore.TrackDateOnly == 1 {
					dateCompleted = time.Date(dateCompleted.Year(), dateCompleted.Month(), dateCompleted.Day(), 0, 0, 0, 0, locale)
				}

				if dateCompleted.After(lastTracked) {
					// update Grocy to track completion
					fmt.Printf("Task %s was completed and should be marked in Grocy\n", getTaskValue(found, "SUMMARY"))
					fmt.Printf("It was found to be completed at: %s\n", getTaskValue(found, "COMPLETED"))

					completedDateTime, err := parseCompletedString(found, chore.TrackDateOnly)
					if err != nil {
						fmt.Printf("Failed to Parse Completion time. %s\n", err)
						break
					}

					fmt.Printf("Tracking execution at %s\n", completedDateTime)
					err = trackExecution(chore.NextExecutionAssignedUser.Id, chore.ChoreId, completedDateTime)
					if err != nil {
						fmt.Printf("Error occurred when tracking execution: %s\n", err)
					}

					// refresh next estimated execution for due date comparison later
					newDetails, err := getChoreDetails(chore.ChoreId)
					if err != nil {
						fmt.Printf("Could refresh chore details: %s\n", err)
					} else {
						fmt.Printf("Updated Next Estimated Execution time: %s\n", newDetails.NextEstimatedExecutionTime)
						chore.NextEstimatedExecutionTime = newDetails.NextEstimatedExecutionTime
					}

					// TODO mark as undone any task that doesn't have a due date
				}
			}

			grocyNext, _ := parseGrocyDate(chore.NextEstimatedExecutionTime)
			// Case 2
			dueDate, err := parseDate(found, "DUE")
			if err != nil {
				fmt.Println(err)
			} else {
				if !dueDate.Equal(grocyNext) {
					// update the DUE date
					fmt.Printf("Updating Due Date of %s from %s to %s\n", chore.ChoreName, dueDate, grocyNext)
					err := setDueDate(*cdClient, found, grocyNext)
					if err != nil {
						fmt.Printf("Failed to set the Due date of %s\n", getTaskValue(found, "SUMMARY"))
					}

					// un-mark todo as completed if it's completed
					completed := getTaskValue(found, "COMPLETED")
					if completed != "" {
						err := unMarkAsCompleted(*cdClient, found)
						if err != nil {
							fmt.Printf("Failed to mark todo as not complete: %s\n", err)
						}
					}
				}
			}

		} else {
			dueStr := strings.Split(chore.NextEstimatedExecutionTime, " ")[0]
			fmt.Printf("Chore %s is not in tasks. Due next: %s\n", chore.ChoreName, dueStr)

			var dueNext time.Time
			if len(chore.NextEstimatedExecutionTime) == 0 {
				fmt.Println("no due date")
				dueNext = time.Time{}
			} else {
				dueNext, err = parseGrocyDate(chore.NextEstimatedExecutionTime)
				if err != nil {
					log.Fatal(err)
				}
			}
			fmt.Println(dueNext)
			fmt.Printf("Creating new todo for: %s\n", chore.ChoreName)
			err = createNewEvent(*cdClient, myTasksCalendar, chore.ChoreName, dueNext)
			if err != nil {
				log.Fatal(err)
			}
		}
	}

}
