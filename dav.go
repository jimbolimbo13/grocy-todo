package main

import (
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/emersion/go-ical"
	webdav "github.com/emersion/go-webdav"
	caldav "github.com/emersion/go-webdav/caldav"
	"github.com/google/uuid"
)

// Caldav commands
func initDavClient(user, pass string) (*caldav.Client, error) {
	wdClient := webdav.HTTPClientWithBasicAuth(nil, user, pass)

	cdClient, err := caldav.NewClient(wdClient, envVars[CALDAV_URL])
	if err != nil {
		return nil, err
	}

	return cdClient, nil
}

func getCaldavTodos(cdClient caldav.Client) (caldav.Calendar, error) {

	calName := "Repeating Tasks"

	principal, err := cdClient.FindCurrentUserPrincipal()
	if err != nil {
		log.Fatal(err)
	}

	homeSet, err := cdClient.FindCalendarHomeSet(principal)
	if err != nil {
		log.Fatal(err)
	}

	calendars, err := cdClient.FindCalendars(homeSet)
	if err != nil {
		log.Fatal(err)
	}

	for _, calendar := range calendars {
		if calendar.Name == calName {
			return calendar, nil
		}
	}

	return caldav.Calendar{}, fmt.Errorf("Could not find '%s' calendar", calName)
}

func getAllTodos(cdClient caldav.Client, calendar caldav.Calendar) ([]caldav.CalendarObject, error) {

	todoQuery := caldav.CalendarQuery{
		CompRequest: caldav.CalendarCompRequest{
			Props: []string{"getetag", "calendar-data"},
		},
		CompFilter: caldav.CompFilter{
			Name: "VCALENDAR",
			Comps: append([]caldav.CompFilter{}, caldav.CompFilter{
				Name: "VTODO",
			}),
		},
	}

	tasks, err := cdClient.QueryCalendar(calendar.Path, &todoQuery)
	if err != nil {
		return []caldav.CalendarObject{}, err
	}

	return tasks, nil
}

// updating a task
// Update requires a ical.Calendar, which is the Data field of a Calendar object
// Children[0] is VTODO

func markAsCompleted(cdClient caldav.Client, task caldav.CalendarObject) error {
	updated := task.Data

	completed := ical.NewProp("COMPLETED")
	completed.SetDateTime(time.Now())
	updated.Children[0].Props["COMPLETED"] = append([]ical.Prop{}, *completed)

	_, err := cdClient.PutCalendarObject(task.Path, updated)
	return err
}

func unMarkAsCompleted(cdClient caldav.Client, task caldav.CalendarObject) error {
	updated := task.Data
	todoProps := task.Data.Children[0].Props
	delete(todoProps, "COMPLETED")
	delete(todoProps, "DTSTART")
	delete(todoProps, "PERCENT-COMPLETE")
	delete(todoProps, "STATUS")

	updated.Children[0].Props = todoProps

	_, err := cdClient.PutCalendarObject(task.Path, updated)
	return err
}

func setDueDate(cdClient caldav.Client, task caldav.CalendarObject, due time.Time) error {
	updated := task.Data

	dueDate := ical.NewProp("DUE")
	dueDate.SetDate(due)
	updated.Children[0].Props["DUE"] = append([]ical.Prop{}, *dueDate)

	_, err := cdClient.PutCalendarObject(task.Path, updated)
	return err
}

func getTaskValue(task caldav.CalendarObject, field string) string {

	if value, exists := task.Data.Children[0].Props[field]; exists {
		return value[0].Value
	}

	return ""
}

func parseDateTime(task caldav.CalendarObject, field string) (time.Time, error) {
	date := getTaskValue(task, field)
	if date == "" {
		return time.Time{}, fmt.Errorf("Could not find a value for field %s", field)
	}

	date = strings.Replace(date, "Z", "", -1)
	dateParsed, err := time.Parse("20060102T150405", date)
	if err != nil {
		return time.Time{}, fmt.Errorf("Failed to parse time from value: %s. %s", date, err)
	}

	return dateParsed.In(locale), nil
}

func parseDate(task caldav.CalendarObject, field string) (time.Time, error) {
	date := getTaskValue(task, field)
	if date == "" {
		return time.Time{}, fmt.Errorf("Could not find a value for field %s", field)
	}
	dateParsed, err := time.Parse("20060102", date)
	if err != nil {
		return time.Time{}, fmt.Errorf("Failed to parse date from value: %s. %s", date, err)
	}

	return time.Date(dateParsed.Year(), dateParsed.Month(), dateParsed.Day(), 0, 0, 0, 0, locale), nil
}

func parseCompletedString(task caldav.CalendarObject, dateOnly int) (string, error) {
	dateParsed, err := parseDateTime(task, "COMPLETED")
	if err != nil {
		return "", err
	}

	if dateOnly == 1 {
		return dateParsed.Format("2006-01-02"), nil
	}

	return dateParsed.Format("2006-01-02 15:04:05"), nil
}

// this is the what the UI sends when recording an execution
// skipped	false
// tracked_time	"2023-05-17 21:51:20"

func createNewEvent(cdClient caldav.Client, calendar caldav.Calendar, name string, due time.Time) error {
	// Set the Path (this is just a UUID)
	uid := uuid.NewString()
	newPath := calendar.Path + uid + ".ics"

	// creating a new event:
	// the Data part:
	// create new ical.Component with name "VCALENDAR"
	newEvent := ical.NewComponent("VCALENDAR")
	// 	set the props:
	// 	 CALSCALE - "GREGORIAN"
	calscale := ical.NewProp("CALSCALE")
	calscale.SetText("GREGORIAN")
	newEvent.Props.Add(calscale)
	// 	 PRODID - "-//Apple Inc.//iOS 16.4.1//EN"
	prodid := ical.NewProp("PRODID")
	prodid.SetText("-//Apple Inc.//iOS 16.4.1//EN")
	newEvent.Props.Add(prodid)
	// 	 VERSION - "2.0"
	version := ical.NewProp("VERSION")
	version.SetText("2.0")
	newEvent.Props.Add(version)

	// 	Children is a list of ical.Components
	// create new ical.Component with name "VTODO" - this will go in list above
	newTodo := ical.NewComponent("VTODO")
	// 	set the props:
	// 	 UID - same uuid as the path
	todoUid := ical.NewProp("UID")
	todoUid.SetText(uid)
	newTodo.Props.Add(todoUid)
	// 	 DUE - based on what Grocy says
	if (due != time.Time{}) {
		todoDue := ical.NewProp("DUE")
		todoDue.SetDate(due)
		newTodo.Props.Add(todoDue)
	}
	//   SUMMARY - the name grom Grocy
	todoSummary := ical.NewProp("SUMMARY")
	todoSummary.SetText(name)
	newTodo.Props.Add(todoSummary)
	//	 DTSTAMP - current timestamp
	todoDtStamp := ical.NewProp("DTSTAMP")
	todoDtStamp.SetDateTime(time.Now())
	newTodo.Props.Add(todoDtStamp)

	// Add todo to Children of event
	newEvent.Children = append([]*ical.Component{}, newTodo)

	// Create the event on the server
	_, err := cdClient.PutCalendarObject(newPath, &ical.Calendar{
		Component: newEvent,
	})
	return err
}
