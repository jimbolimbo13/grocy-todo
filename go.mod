module main

go 1.18

require (
	github.com/emersion/go-ical v0.0.0-20220601085725-0864dccc089f
	github.com/emersion/go-webdav v0.4.0
	github.com/google/uuid v1.3.0
)

require github.com/teambition/rrule-go v1.7.2 // indirect
