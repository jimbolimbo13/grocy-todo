# Sync chores in Grocy to a CalDav todo list

After searching around for a habit tracker, I never found one I really liked. Until I found... [Grocy](https://grocy.info/)?

Yep, the "chores" functionality is meant to track and manage repeated tasks, and handles days when the habit is missed or skipped.

 - ( side note, grocy is a really amazing application for managing all sorts of things at home, definitely check it out for more than just this )

Connecting the chores to a caldav todo list lets me interact with what needs to be done "today" more easily. e.g. I can check them off from my nextcloud dashboard, or my phone's built-in reminders app.

If you're just looking for a better interface to grocy from an iphone, check out [this shortcut](https://routinehub.co/shortcut/7061/) or this [iOS app](https://github.com/supergeorg/Grocy-SwiftUI).

I was also looking for an easy coding project to practice Go, and this fit the bill.

# How to run

Get an api key for your grocy server: https://your.grocy.url/manageapikeys

copy .env-example to .env and add your secrets
- grocy-todo also reads secrets from environment variables

run with `docker run --env-file .env --rm -it registry.gitlab.com/jimbolimbo13/grocy-todo:latest`

Intended to be run once daily (around midnight or so) to sync the tasks for the day and update caldav with what needs to be un-checked.

### Periodic run

Running daily could be done with cron, systemd timers, or other tools
see example systemd timer.

To enable the timer: 
- Update the path in docker-run.sh for the .env file location
- Update the path in grocy-todo.service to point to the docker-run.sh file

- Install the timer
`sudo cp grocy-todo.service /etc/systemd/system/`
`sudo cp grocy-todo.timer /etc/systemd/system/`

- Refresh systemd units
`sudo systemctl daemon-reload`

- Turn on the timer
`sudo systemctl enable grocy-todo.timer`
`sudo systemctl start grocy-todo.timer`

# Build and deploy the container
export VERSION or update the value in the call: 
`docker build -t registry.gitlab.com/jimbolimbo13/grocy-todo:latest -t  registry.gitlab.com/jimbolimbo13/grocy-todo:$VERSION .`

`docker login -u jimbolimbo13 registry.gitlab.com`


`docker push registry.gitlab.com/jimbolimbo13/grocy-todo:latest`
`docker push registry.gitlab.com/jimbolimbo13/grocy-todo:$VERSION`
